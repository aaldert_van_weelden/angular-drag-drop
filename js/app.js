var app = angular.module('plunker', ['ngDragDrop']);

app.controller('MainCtrl', function($scope,$rootScope) {
  $scope.men = [
      'John',
      'Jack',
      'Mark',
      'Ernie'
      ];
      
      
      $scope.women = [
      'Jane',
      'Jill',
      'Betty',
      'Mary'
      ];
      
      
      $scope.addText = "";
      
      
      $scope.dropSuccessHandler = function($event,index,array){
          array.splice(index,1);
      };
      
      $scope.onDrop = function($event,$data,array){
          array.push($data);
      };
      
      
      $rootScope.$on("ANGULAR_DRAG_START", function (event, channel,data) {
          console.debug("ANGULAR_DRAG_START: " + channel + ", data: " +data);
      });



      $rootScope.$on("ANGULAR_DRAG_END", function (event, channel) {
          console.debug('ANGULAR_DRAG_END: '+channel);
      });
  
});
